#include "file_lister.h"

std::map<std::string, std::vector<std::string>> sha256_map;

void find_duplicates(std::string &p, std::string &md)
{
        auto it = sha256_map.find(md);

        if (it != sha256_map.end()) {
                auto v = it->second;
                std::cout << p << " has dups:" << std::endl;
                for (auto iv = v.begin(); iv != v.end(); ++iv)
                        std::cout << "\t-> " << *iv << std::endl;
        }

        sha256_map[md].push_back(p);
}


void list_f_sha256(std::string &path, int chunk_size)
{
        struct dirent *entry;
        DIR *d = opendir(path.c_str());

        if (!d)
                return;

        if (path[path.size() -1] == '/')
                path.erase(path.size() -1, 1);

        while ((entry = readdir(d)) != NULL) {
                std::string file_name = entry->d_name;

                if (file_name == "." || file_name == "..")
                        continue;

                std::string full_path = path + "/" + file_name;

                if (entry->d_type != DT_DIR) {
                        std::string digest;

                        compute_file_sha256(full_path, digest, chunk_size);
                        find_duplicates(full_path, digest);
                        continue;
                } else
                        list_f_sha256(full_path);
        }
}
