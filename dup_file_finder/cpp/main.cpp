#include <iostream>
#include <stdlib.h>
#include "file_lister.h"

int main(int argc, char **argv)
{
        clock_t start = clock();
        std::string path = argv[1];
        int chunk_size = atoi(argv[2]);

        list_f_sha256(path, chunk_size);

        std::cout << "Time taken: " 
                << ((double)(clock() - start)/CLOCKS_PER_SEC)
                << " seconds...";
        return 0;
}
