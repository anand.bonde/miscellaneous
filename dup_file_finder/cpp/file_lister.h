#ifndef __FILE_LISTER
#define __FILE_LISTER

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <dirent.h>
#include <sys/types.h>
#include "sha256.h"

void list_f_sha256(std::string &path, int chunk_size = 32768);

#endif //__FILE_LISTER
