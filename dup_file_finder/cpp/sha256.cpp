#include "sha256.h"

void sha256(const std::string &string, std::string &digest)
{
        unsigned char md[SHA256_DIGEST_LENGTH];

        SHA256_CTX ctx;
        SHA256_Init(&ctx);
        SHA256_Update(&ctx, string.c_str(), string.size());
        SHA256_Final(md, &ctx);

        char mdString[SHA256_DIGEST_LENGTH*2+1];
        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
                sprintf(&mdString[i*2], "%02x", md[i]);

        digest = mdString;
}

void compute_file_sha256(const std::string &path, std::string &digest, int chunk_size = 32768)
{
        FILE *file = fopen(path.c_str(), "rb");
        if(!file)
                return;

        unsigned char hash[SHA256_DIGEST_LENGTH];
        SHA256_CTX sha256;
        SHA256_Init(&sha256);

        int bytesRead = 0;
        unsigned char *buffer = (unsigned char *)malloc(chunk_size);

        while((bytesRead = fread(buffer, 1, chunk_size, file)))
                SHA256_Update(&sha256, buffer, bytesRead);

        free(buffer);
        fclose(file);

        SHA256_Final(hash, &sha256);

        char mdString[SHA256_DIGEST_LENGTH*2+1];

        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
                sprintf(&mdString[i*2], "%02x", hash[i]);

        digest = mdString;
}
