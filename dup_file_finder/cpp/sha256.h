#ifndef __SHA256
#define __SHA256

#include <string>
#include <iomanip>
#include <openssl/sha.h>

void sha256(const std::string &string, std::string &digest);
void compute_file_sha256(const std::string &path, std::string &digest, int chunk_size);

#endif //__SHA256
