#ifndef __SHA256
#define __SHA256

#include <stdio.h>
#include <stdlib.h>
#include <openssl/sha.h>

char *sha256(const unsigned char *s, unsigned int len);

#endif // __SHA256
