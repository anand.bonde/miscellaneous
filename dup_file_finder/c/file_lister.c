#include "file_lister.h"

void compute_sha256(char *path,  int level)
{
        DIR *d;
        char *t1,*t2;
        struct dirent *entry;
        const unsigned char *s;
        char recursive_path[PATH_MAX];

        printf("\n\ndir: %s, level = %d\n", path, level);
        d = opendir(path);
        if (!d)
                return;

        while ((entry = readdir(d)) != NULL) {
                if (strcmp(entry->d_name, ".") == 0)
                        continue;
                if (strcmp(entry->d_name, "..") == 0) 
                        continue;
                if (entry->d_type != DT_DIR) {
                        s = (const unsigned char *)entry->d_name;
                        char *sha256_hash = sha256(s, strlen(entry->d_name));
                        printf("\n\t%-10s\tsha256:\t%s", entry->d_name, sha256_hash);
                        free(sha256_hash);
                        continue;
                }

                snprintf(recursive_path, PATH_MAX, "%s/%s", path, 
                                entry->d_name);

                compute_sha256(recursive_path, level + 1);
        }

        closedir(d);
}
