#include "sha256.h"

char *sha256(const unsigned char *s, unsigned int len)
{
        unsigned char *d = SHA256(s, len, 0);
        char *md = (char *)malloc(SHA256_DIGEST_LENGTH*2+1);

        md[SHA256_DIGEST_LENGTH*2] = '\0';

        for (unsigned int i = 0; i < SHA256_DIGEST_LENGTH; i++)
                sprintf(&md[i*2], "%02x", (unsigned int)d[i]);

        return md;
}
