#include <stdio.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <openssl/sha.h>
#include "sha256.h"
#include "file_lister.h"

int main(int argc, char **argv)
{
        clock_t t = clock();

        compute_sha256(argv[1], 0);
        t = clock() - t;
        printf("\n\ntotal time: %f\n", ((double)t)/CLOCKS_PER_SEC);
        return 0;
}
