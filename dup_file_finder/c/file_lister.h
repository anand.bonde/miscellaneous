#ifndef __FILE_LISTER
#define __FILE_LISTER

#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "sha256.h"

void compute_sha256(char *path,  int level);

#endif // __FILE_LISTER
