[
    {
        "id": "bd68d509.594148",
        "type": "tab",
        "label": "Home View",
        "disabled": false,
        "info": ""
    },
    {
        "id": "74ae769.8179b88",
        "type": "tab",
        "label": "Home Control",
        "disabled": false,
        "info": ""
    },
    {
        "id": "7212969e.919928",
        "type": "ui_group",
        "z": "",
        "name": "Raspberry Pi Monitor",
        "tab": "13382752.4c2ae9",
        "order": 2,
        "disp": true,
        "width": "6"
    },
    {
        "id": "eb4c566d.a0b9b8",
        "type": "ui_base",
        "theme": {
            "name": "theme-dark",
            "lightTheme": {
                "default": "#0094CE",
                "baseColor": "#0094CE",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "edited": true,
                "reset": false
            },
            "darkTheme": {
                "default": "#097479",
                "baseColor": "#097479",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "edited": true,
                "reset": false
            },
            "customTheme": {
                "name": "Untitled Theme 1",
                "default": "#4B7930",
                "baseColor": "#4B7930",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
            },
            "themeState": {
                "base-color": {
                    "default": "#097479",
                    "value": "#097479",
                    "edited": false
                },
                "page-titlebar-backgroundColor": {
                    "value": "#097479",
                    "edited": false
                },
                "page-backgroundColor": {
                    "value": "#111111",
                    "edited": false
                },
                "page-sidebar-backgroundColor": {
                    "value": "#000000",
                    "edited": false
                },
                "group-textColor": {
                    "value": "#0eb8c0",
                    "edited": false
                },
                "group-borderColor": {
                    "value": "#555555",
                    "edited": false
                },
                "group-backgroundColor": {
                    "value": "#333333",
                    "edited": false
                },
                "widget-textColor": {
                    "value": "#eeeeee",
                    "edited": false
                },
                "widget-backgroundColor": {
                    "value": "#097479",
                    "edited": false
                },
                "widget-borderColor": {
                    "value": "#333333",
                    "edited": false
                },
                "base-font": {
                    "value": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
                }
            }
        },
        "site": {
            "name": "Node-RED Dashboard",
            "hideToolbar": "false",
            "allowSwipe": "true",
            "dateFormat": "DD/MM/YYYY",
            "sizes": {
                "sx": 48,
                "sy": 48,
                "gx": 6,
                "gy": 6,
                "cx": 6,
                "cy": 6,
                "px": 0,
                "py": 0
            }
        }
    },
    {
        "id": "13382752.4c2ae9",
        "type": "ui_tab",
        "z": "",
        "name": "Home View",
        "icon": "dashboard",
        "order": 1
    },
    {
        "id": "35b6a3df.e2c77c",
        "type": "ui_group",
        "z": "",
        "name": "Network Monitor",
        "tab": "13382752.4c2ae9",
        "order": 3,
        "disp": true,
        "width": "6"
    },
    {
        "id": "d27b3e97.07498",
        "type": "ui_group",
        "z": "",
        "name": "My Portal Control",
        "tab": "13382752.4c2ae9",
        "order": 4,
        "disp": true,
        "width": "6"
    },
    {
        "id": "69f2c51b.bd5b0c",
        "type": "ui_group",
        "z": "",
        "name": "Spooky Control",
        "tab": "13382752.4c2ae9",
        "order": 5,
        "disp": true,
        "width": "6"
    },
    {
        "id": "4be3e974.493ac8",
        "type": "ui_group",
        "z": "",
        "name": "Weather Overview",
        "tab": "13382752.4c2ae9",
        "order": 1,
        "disp": true,
        "width": "6"
    },
    {
        "id": "376374a9.543aac",
        "type": "ui_group",
        "z": "",
        "name": "Bedroom Light",
        "tab": "13382752.4c2ae9",
        "disp": true,
        "width": "6"
    },
    {
        "id": "e1b15989.6a6598",
        "type": "ui_group",
        "z": "",
        "name": "Surveillance",
        "tab": "13382752.4c2ae9",
        "disp": true,
        "width": "6",
        "collapse": false
    },
    {
        "id": "ecea6e04.3174b",
        "type": "inject",
        "z": "bd68d509.594148",
        "name": "Every minute",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "60",
        "crontab": "",
        "once": true,
        "x": 120,
        "y": 60,
        "wires": [
            [
                "d53b6a41.9f08b8"
            ]
        ]
    },
    {
        "id": "d53b6a41.9f08b8",
        "type": "exec",
        "z": "bd68d509.594148",
        "command": "vcgencmd measure_temp",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Get Raspberry Pi Core Temperature",
        "x": 427,
        "y": 60,
        "wires": [
            [
                "51b4f175.0be7b"
            ],
            [],
            []
        ]
    },
    {
        "id": "51b4f175.0be7b",
        "type": "function",
        "z": "bd68d509.594148",
        "name": "Extract Temperature Value",
        "func": "var temp= msg.payload.substring(5,9);\nmsg.payload = temp;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 797,
        "y": 60,
        "wires": [
            [
                "f0f61b59.581e98",
                "8d6e98a6.8b8908"
            ]
        ]
    },
    {
        "id": "f0f61b59.581e98",
        "type": "ui_gauge",
        "z": "bd68d509.594148",
        "name": "Raspberry Pi Core Temperature",
        "group": "7212969e.919928",
        "order": 0,
        "width": 0,
        "height": 0,
        "gtype": "gage",
        "title": "Raspberry Pi Core Temperature",
        "label": "�C",
        "format": "{{value}}",
        "min": "35",
        "max": "70",
        "colors": [
            "#00b500",
            "#e6e600",
            "#ca3838"
        ],
        "seg1": "",
        "seg2": "",
        "x": 1110,
        "y": 40,
        "wires": []
    },
    {
        "id": "8d6e98a6.8b8908",
        "type": "ui_chart",
        "z": "bd68d509.594148",
        "name": "Raspberry Pi Core Temperature History",
        "group": "7212969e.919928",
        "order": 0,
        "width": 0,
        "height": 0,
        "label": "Core Temperature History",
        "chartType": "line",
        "legend": "false",
        "xformat": "HH:mm",
        "interpolate": "linear",
        "nodata": "Core Temperature History",
        "dot": false,
        "ymin": "40",
        "ymax": "80",
        "removeOlder": "24",
        "removeOlderPoints": "",
        "removeOlderUnit": "3600",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#1f77b4",
            "#aec7e8",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 1140,
        "y": 80,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "5573f672.e175d8",
        "type": "inject",
        "z": "bd68d509.594148",
        "name": "Every minute",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "60",
        "crontab": "",
        "once": true,
        "x": 120,
        "y": 240,
        "wires": [
            [
                "56f88920.360188"
            ]
        ]
    },
    {
        "id": "56f88920.360188",
        "type": "exec",
        "z": "bd68d509.594148",
        "command": "sudo arp-scan -l | grep 10.0.0. | wc -l",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Get #Connected Devices",
        "x": 410,
        "y": 240,
        "wires": [
            [
                "60b47f9.b90858",
                "40d3769c.8643d8"
            ],
            [],
            []
        ]
    },
    {
        "id": "60b47f9.b90858",
        "type": "ui_gauge",
        "z": "bd68d509.594148",
        "name": "#Connected Devices History",
        "group": "35b6a3df.e2c77c",
        "order": 0,
        "width": 0,
        "height": 0,
        "gtype": "gage",
        "title": "#Connected Devices History",
        "label": "devices",
        "format": "{{value}}",
        "min": "0",
        "max": "25",
        "colors": [
            "#00b500",
            "#e6e600",
            "#ca3838"
        ],
        "seg1": "",
        "seg2": "",
        "x": 1100,
        "y": 220,
        "wires": []
    },
    {
        "id": "40d3769c.8643d8",
        "type": "ui_chart",
        "z": "bd68d509.594148",
        "name": "#Connected Devices",
        "group": "35b6a3df.e2c77c",
        "order": 0,
        "width": 0,
        "height": 0,
        "label": "#Connected Devices",
        "chartType": "line",
        "legend": "false",
        "xformat": "HH:mm",
        "interpolate": "linear",
        "nodata": "#Connected Devices",
        "dot": false,
        "ymin": "0",
        "ymax": "20",
        "removeOlder": "24",
        "removeOlderPoints": "",
        "removeOlderUnit": "3600",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#1f77b4",
            "#aec7e8",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 1080,
        "y": 260,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "85ec7e5a.97533",
        "type": "exec",
        "z": "74ae769.8179b88",
        "command": "cd /home/pi/git/komand_center && sudo python3 manage.py runserver 0.0.0.0:80 &",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Start My Portal",
        "x": 620,
        "y": 140,
        "wires": [
            [],
            [],
            []
        ]
    },
    {
        "id": "15f3b45b.17566c",
        "type": "alexa-local",
        "z": "74ae769.8179b88",
        "devicename": "My Portal",
        "inputtrigger": false,
        "x": 160,
        "y": 60,
        "wires": [
            [
                "bbcbbdc9.6df18"
            ]
        ]
    },
    {
        "id": "bbcbbdc9.6df18",
        "type": "switch",
        "z": "74ae769.8179b88",
        "name": "OnOffSwitch",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "on",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "off",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 390,
        "y": 140,
        "wires": [
            [
                "85ec7e5a.97533",
                "c530f4de.5685c8"
            ],
            [
                "bc721027.d8d7a",
                "c530f4de.5685c8"
            ]
        ]
    },
    {
        "id": "bc721027.d8d7a",
        "type": "exec",
        "z": "74ae769.8179b88",
        "command": "sudo pkill -9 python3",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Stop My Portal",
        "x": 620,
        "y": 200,
        "wires": [
            [],
            [],
            []
        ]
    },
    {
        "id": "e9ccf2c8.132a",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "On",
        "group": "d27b3e97.07498",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "On",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "on",
        "payloadType": "str",
        "topic": "",
        "x": 170,
        "y": 140,
        "wires": [
            [
                "bbcbbdc9.6df18"
            ]
        ]
    },
    {
        "id": "fc301188.e0329",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "Off",
        "group": "d27b3e97.07498",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Off",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "off",
        "payloadType": "str",
        "topic": "",
        "x": 170,
        "y": 200,
        "wires": [
            [
                "bbcbbdc9.6df18"
            ]
        ]
    },
    {
        "id": "c530f4de.5685c8",
        "type": "ui_text",
        "z": "74ae769.8179b88",
        "group": "d27b3e97.07498",
        "order": 0,
        "width": 0,
        "height": 0,
        "name": "My Portal Status",
        "label": "Status",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 630,
        "y": 60,
        "wires": []
    },
    {
        "id": "8346798c.9b48c8",
        "type": "wunderground in",
        "z": "bd68d509.594148",
        "name": "Weather Underground",
        "lon": "",
        "lat": "",
        "city": "Redmond",
        "country": "Washington",
        "x": 120,
        "y": 420,
        "wires": [
            [
                "9dc28a67.4eec48",
                "3a2d342c.41a80c"
            ]
        ]
    },
    {
        "id": "30fa4756.f8afc8",
        "type": "ui_gauge",
        "z": "bd68d509.594148",
        "name": "Outside Temperature",
        "group": "4be3e974.493ac8",
        "order": 1,
        "width": 0,
        "height": 0,
        "gtype": "gage",
        "title": "Outside Temperature",
        "label": "�C",
        "format": "{{value}}",
        "min": "0",
        "max": "50",
        "colors": [
            "#00b500",
            "#e6e600",
            "#ca3838"
        ],
        "seg1": "",
        "seg2": "",
        "x": 1088.5,
        "y": 382.5,
        "wires": []
    },
    {
        "id": "252fd39.8c2942c",
        "type": "ui_chart",
        "z": "bd68d509.594148",
        "name": "Outside Temperature History",
        "group": "4be3e974.493ac8",
        "order": 2,
        "width": 0,
        "height": 0,
        "label": "Outside Temperature History",
        "chartType": "line",
        "legend": "false",
        "xformat": "HH:mm",
        "interpolate": "linear",
        "nodata": "Outside Temperature History",
        "dot": false,
        "ymin": "-15",
        "ymax": "40",
        "removeOlder": "24",
        "removeOlderPoints": "",
        "removeOlderUnit": "3600",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#1f77b4",
            "#aec7e8",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 1100,
        "y": 420,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "9dc28a67.4eec48",
        "type": "function",
        "z": "bd68d509.594148",
        "name": "Extract Forecast",
        "func": "msg.payload = msg.payload.forecast;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 390,
        "y": 460,
        "wires": [
            [
                "bd49ac48.0a00c"
            ]
        ]
    },
    {
        "id": "bd49ac48.0a00c",
        "type": "ui_text",
        "z": "bd68d509.594148",
        "group": "4be3e974.493ac8",
        "order": 3,
        "width": "6",
        "height": "2",
        "name": "Forecast",
        "label": "",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 1040,
        "y": 460,
        "wires": []
    },
    {
        "id": "444c6e1f.c26b2",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "On",
        "group": "376374a9.543aac",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "On",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "on",
        "payloadType": "str",
        "topic": "",
        "x": 171,
        "y": 342,
        "wires": [
            [
                "d699a39e.99c6d",
                "41ad6e1c.41673"
            ]
        ]
    },
    {
        "id": "8cff11bc.3bc6d",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "Off",
        "group": "376374a9.543aac",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Off",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "off",
        "payloadType": "str",
        "topic": "",
        "x": 172,
        "y": 405,
        "wires": [
            [
                "d699a39e.99c6d",
                "41ad6e1c.41673"
            ]
        ]
    },
    {
        "id": "41ad6e1c.41673",
        "type": "ui_text",
        "z": "74ae769.8179b88",
        "group": "376374a9.543aac",
        "order": 0,
        "width": 0,
        "height": 0,
        "name": "Bedroom Light Status",
        "label": "Status",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 647,
        "y": 324,
        "wires": []
    },
    {
        "id": "d699a39e.99c6d",
        "type": "hs100",
        "z": "74ae769.8179b88",
        "name": "Bedroom Light",
        "host": "10.0.0.249",
        "x": 629.5,
        "y": 392,
        "wires": [
            []
        ]
    },
    {
        "id": "3a2d342c.41a80c",
        "type": "function",
        "z": "bd68d509.594148",
        "name": "Extract Temperature",
        "func": "msg.payload = msg.payload.tempc;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 400,
        "y": 400,
        "wires": [
            [
                "30fa4756.f8afc8",
                "252fd39.8c2942c"
            ]
        ]
    },
    {
        "id": "d1c09924.ffad08",
        "type": "alexa-local",
        "z": "74ae769.8179b88",
        "devicename": "Spooky",
        "inputtrigger": false,
        "x": 160,
        "y": 500,
        "wires": [
            [
                "270a80cf.834ee"
            ]
        ]
    },
    {
        "id": "270a80cf.834ee",
        "type": "switch",
        "z": "74ae769.8179b88",
        "name": "OnOffSwitch",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "on",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "off",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "outputs": 2,
        "x": 390,
        "y": 580,
        "wires": [
            [
                "b1743f25.cadee",
                "da35e851.1abce8"
            ],
            [
                "b1743f25.cadee"
            ]
        ]
    },
    {
        "id": "d7433a8c.7bfad8",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "On",
        "group": "69f2c51b.bd5b0c",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "On",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "on",
        "payloadType": "str",
        "topic": "",
        "x": 170,
        "y": 580,
        "wires": [
            [
                "270a80cf.834ee"
            ]
        ]
    },
    {
        "id": "1b15b205.7d1bae",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "Off",
        "group": "69f2c51b.bd5b0c",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Off",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "off",
        "payloadType": "str",
        "topic": "",
        "x": 170,
        "y": 640,
        "wires": [
            [
                "270a80cf.834ee"
            ]
        ]
    },
    {
        "id": "b1743f25.cadee",
        "type": "ui_text",
        "z": "74ae769.8179b88",
        "group": "69f2c51b.bd5b0c",
        "order": 0,
        "width": 0,
        "height": 0,
        "name": "Spooky Status",
        "label": "Status",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 630,
        "y": 500,
        "wires": []
    },
    {
        "id": "da35e851.1abce8",
        "type": "wake on lan",
        "z": "74ae769.8179b88",
        "mac": "84:3a:4b:0b:3d:5c",
        "host": "10.0.0.25",
        "name": "Wake up Spooky",
        "x": 650,
        "y": 580,
        "wires": []
    },
    {
        "id": "2067a3d5.753b6c",
        "type": "http in",
        "z": "bd68d509.594148",
        "name": "/squirrel_is_home",
        "url": "/squirrel_is_home",
        "method": "post",
        "upload": false,
        "swaggerDoc": "",
        "x": 150,
        "y": 580,
        "wires": [
            [
                "ed09a8e6.7750b8",
                "c651ad93.6c78c"
            ]
        ]
    },
    {
        "id": "5667c369.7c609c",
        "type": "http in",
        "z": "bd68d509.594148",
        "name": "/anand_is_home",
        "url": "/anand_is_home",
        "method": "post",
        "upload": false,
        "swaggerDoc": "",
        "x": 160,
        "y": 680,
        "wires": [
            [
                "c651ad93.6c78c",
                "57b46d73.6b29b4"
            ]
        ]
    },
    {
        "id": "d23c0109.84f17",
        "type": "exec",
        "z": "bd68d509.594148",
        "command": "/home/pi/git/tts/speech.sh",
        "addpay": true,
        "append": "Hi Komal, welcome home! Hope you had a pleasant day at work!",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Speak Out",
        "x": 710,
        "y": 860,
        "wires": [
            [],
            [],
            []
        ]
    },
    {
        "id": "5f8aa4c0.19bedc",
        "type": "http in",
        "z": "bd68d509.594148",
        "name": "/komal_is_home",
        "url": "/komal_is_home",
        "method": "post",
        "upload": false,
        "swaggerDoc": "",
        "x": 160,
        "y": 780,
        "wires": [
            [
                "d23c0109.84f17",
                "c651ad93.6c78c",
                "fe75c70c.34f4a8"
            ]
        ]
    },
    {
        "id": "c651ad93.6c78c",
        "type": "http response",
        "z": "bd68d509.594148",
        "name": "Ok response",
        "statusCode": "200",
        "headers": {
            "response": "thank_you"
        },
        "x": 350,
        "y": 640,
        "wires": []
    },
    {
        "id": "f40b607f.4fd62",
        "type": "switch",
        "z": "74ae769.8179b88",
        "name": "OnOffSwitch",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "on",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "off",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "outputs": 2,
        "x": 390,
        "y": 820,
        "wires": [
            [
                "37c71119.0d3f8e"
            ],
            [
                "ca46fbc7.7066c8"
            ]
        ]
    },
    {
        "id": "126ca7f3.f01368",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "On",
        "group": "e1b15989.6a6598",
        "order": 1,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "On",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "on",
        "payloadType": "str",
        "topic": "",
        "x": 170,
        "y": 840,
        "wires": [
            [
                "f40b607f.4fd62"
            ]
        ]
    },
    {
        "id": "8f88d169.7fab",
        "type": "ui_button",
        "z": "74ae769.8179b88",
        "name": "Off",
        "group": "e1b15989.6a6598",
        "order": 2,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Off",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "off",
        "payloadType": "str",
        "topic": "",
        "x": 170,
        "y": 900,
        "wires": [
            [
                "f40b607f.4fd62"
            ]
        ]
    },
    {
        "id": "37c71119.0d3f8e",
        "type": "exec",
        "z": "74ae769.8179b88",
        "command": "sudo motion",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Start Surveillance (Motion Capture)",
        "x": 720,
        "y": 800,
        "wires": [
            [],
            [
                "ca4f0fb1.51b0b"
            ],
            []
        ]
    },
    {
        "id": "ca46fbc7.7066c8",
        "type": "exec",
        "z": "74ae769.8179b88",
        "command": "sudo pkill -9 motion",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "Stop Surveillance (Motion Capture)",
        "x": 720,
        "y": 860,
        "wires": [
            [],
            [
                "ca4f0fb1.51b0b"
            ],
            []
        ]
    },
    {
        "id": "ca4f0fb1.51b0b",
        "type": "debug",
        "z": "74ae769.8179b88",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 980,
        "y": 760,
        "wires": []
    },
    {
        "id": "8e01510c.13f01",
        "type": "alexa-local",
        "z": "74ae769.8179b88",
        "devicename": "Squirrel Cam",
        "inputtrigger": false,
        "x": 160,
        "y": 760,
        "wires": [
            [
                "f40b607f.4fd62"
            ]
        ]
    },
    {
        "id": "ffeee0a1.c7336",
        "type": "ui_text",
        "z": "bd68d509.594148",
        "group": "e1b15989.6a6598",
        "order": 3,
        "width": "0",
        "height": "0",
        "name": "Komal's presence",
        "label": "Komal's presence",
        "format": "{{msg.payload.status}}",
        "layout": "row-spread",
        "x": 1070,
        "y": 780,
        "wires": []
    },
    {
        "id": "40b29b37.f825d4",
        "type": "ui_text",
        "z": "bd68d509.594148",
        "group": "e1b15989.6a6598",
        "order": 3,
        "width": "0",
        "height": "0",
        "name": "Anand's presence",
        "label": "Anand's presence",
        "format": "{{msg.payload.status}}",
        "layout": "row-spread",
        "x": 1070,
        "y": 680,
        "wires": []
    },
    {
        "id": "34ecf265.58bb9e",
        "type": "ui_text",
        "z": "bd68d509.594148",
        "group": "e1b15989.6a6598",
        "order": 3,
        "width": "0",
        "height": "0",
        "name": "Squirrel's presence",
        "label": "Squirrel's presence",
        "format": "{{msg.payload.status}}",
        "layout": "row-spread",
        "x": 1070,
        "y": 580,
        "wires": []
    },
    {
        "id": "ed09a8e6.7750b8",
        "type": "json",
        "z": "bd68d509.594148",
        "name": "Json String -> Object",
        "property": "payload",
        "action": "",
        "pretty": false,
        "x": 500,
        "y": 580,
        "wires": [
            [
                "34ecf265.58bb9e"
            ]
        ]
    },
    {
        "id": "57b46d73.6b29b4",
        "type": "json",
        "z": "bd68d509.594148",
        "name": "Json String -> Object",
        "property": "payload",
        "action": "",
        "pretty": false,
        "x": 500,
        "y": 680,
        "wires": [
            [
                "40b29b37.f825d4"
            ]
        ]
    },
    {
        "id": "fe75c70c.34f4a8",
        "type": "json",
        "z": "bd68d509.594148",
        "name": "Json String -> Object",
        "property": "payload",
        "action": "",
        "pretty": false,
        "x": 500,
        "y": 780,
        "wires": [
            [
                "ffeee0a1.c7336"
            ]
        ]
    }
]
