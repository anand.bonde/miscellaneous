#!/bin/bash

echo "building kernel identical to the currently running one"
echo "please be around for answering configuration propmpts, just hit enter if unsure"

echo "get running config from the distro"
cp /boot/config-`uname -r`* .config

echo "building kernel, modules and installing"
make -j5 && sudo make modules_install install

echo "updating the grub boot loader to use the freshly built kernel"
sudo update-grub2

echo "ready! you may reboot and enjoy the new kernel now!"
