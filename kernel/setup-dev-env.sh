#!/bin/bash
echo "setting up kernel development environment"
echo "this may take a while (please be around for answering prompts)"

echo "updating system for first use"
sudo apt update -y
sudo apt upgrade -y
sudo apt dist-upgrade  -y
sudo apt autoremove -y

echo "installing tools"
sudo apt install -y vim libncurses5-dev gcc make git exuberant-ctags libssl-dev
sudo apt install -y flex bison libelf-dev
sudo apt install -y git gitk
sudo apt install -y mutt esmtp

echo "cloning git repo for env configs"
mkdir -p ~/playground/git
cd ~/playground/git
git clone https://github.com/anandbonde/miscellaneous.git

echo "copying configs to their respective locations"
cd ~/playground/git/miscellaneous/kernel
cp .muttrc ~
cp .gitconfig ~
cp .vimrc ~
cp .esmtprc ~
echo "you need to change the password in the ~/.esmtprc file"

echo "getting kernel source code (go take a coffee and come back)"
mkdir -p ~/playground/git/kernels
cd ~/playground/git/kernels
git clone git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git

echo "creating a new branch (dev-anand)"
cd ~/playground/git/kernels/linux-next
git checkout -b dev-anand
make tags
git status

echo "your environment is ready! hack on!"
