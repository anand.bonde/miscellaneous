syn on
set ai
set rnu
set ts=8
set sw=8
set expandtab

call plug#begin('~/.vim/plugged')
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'valloric/youcompleteme'
	Plug 'mattn/emmet-vim'
	Plug 'klen/python-mode'
	Plug 'mptre/vim-printf'
	Plug 'bling/vim-airline'
	Plug 'kien/ctrlp.vim'
call plug#end()
