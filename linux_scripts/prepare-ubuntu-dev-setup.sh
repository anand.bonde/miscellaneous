#!/bin/bash
git_code_local_dir="/home/anand/git/"
bashrc_file_path="/home/anand/.bashrc"

mkdir $git_code_local_dir
cd $git_code_local_dir

echo "setting up ppas"
sudo add-apt-repository ppa:regolith-linux/release

echo "installing necessary software"
sudo apt install -y vim git openssh-server curl cmake virtualenv virtualenvwrapper python3-pip python-dev \
                    python3-dev tlp nodejs npm regolith-desktop libxml2-dev libcurl4-openssl-dev \
                    libssl-dev

sudo apt install -y tree figlet rofi ranger vifm sxhkd

sudo apt install -y i3lock

echo "starting services"
sudo tlp start

echo "getting vimplug"
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "cloning git repositories"
git clone "https://github.com/anandbonde/miscellaneous.git"

echo "copying config."
cp /home/anand/git/miscellaneous/raspberrypi_configs/.vimrc ~/.vimrc

echo "updating the system, this may take while"
./miscellaneous/linux_scripts/upd.sh

echo "building completer support for C"
cd ~/.vim/plugged/youcompleteme
./install.py --all
cd

echo
echo "setting up terminal for future use"
echo "" >> $bashrc_file_path
echo "# added by Anand Bonde" >> $bashrc_file_path
echo "alias v=vim" >> $bashrc_file_path
echo "alias vt='vim -p'" >> $bashrc_file_path
echo "alias u='~/git/miscellaneous/linux_scripts/upd.sh'" >> $bashrc_file_path
echo "set -o vi" >> $bashrc_file_path

echo "creating and activating virtualenv"
echo "source /usr/share/virtualenvwrapper/virtualenvwrapper.sh" >> $bashrc_file_path
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
mkvirtualenv myenv -p /usr/bin/python3.7

echo "setting up aliases"
# jupyter related aliases
echo "alias jl='jupyter lab'" >> $bashrc_file_path
echo "alias jn='jupyter notebook'" >> $bashrc_file_path

# git aliases
echo "alias gs='git status'" >> $bashrc_file_path
echo "alias gp='git pull'" >> $bashrc_file_path
echo "alias gd='git diff'" >> $bashrc_file_path

# face recognition
echo "alias ctr='cd ~/git/face_detector/python/src/ && python generate_training_set.py && python generate_model.py && python face_recognizer.py'" >> $bashrc_file_path
echo "alias tr='cd ~/git/face_detector/python/src/ && python generate_model.py && python face_recognizer.py'" >> $bashrc_file_path
echo "alias fr='cd ~/git/face_detector/python/src/ && python face_recognizer.py'" >> $bashrc_file_path
echo "alias clean_fd='rm -rf cd ~/git/face_detector/python/models/* cd ~/git/face_detector/python/training_data/*'" >> $bashrc_file_path

echo "attempting to apply these settings to the current terminal instance"
source $bashrc_file_path

ip addr

echo "all done"
echo "restart the terminal for settings to take effect for sure!"

