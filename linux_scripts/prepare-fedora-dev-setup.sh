#!/bin/bash
git_code_local_dir="/home/anand/git/"
bashrc_file_path="/home/anand/.bashrc"

echo
echo "*** installing necessary software..."
echo "*** essentials..."
sudo dnf install -y vim git openssh-server curl cmake

echo
echo "*** getting vimplug..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo
echo "*** dev. packages..."
sudo dnf install -y python-devel python3-devel python3-virtualenvwrapper python3-pip gcc-c++

echo
echo "*** switching to python3.7..."
sudo unlink /usr/bin/python
sudo ln -s /usr/bin/python3.7 /usr/bin/python

echo
echo "*** installing opencv libraries..."
pip3.7 install opencv-python

echo
echo "*** installing power saver tools..."
sudo dnf install -y tlp
sudo tlp start

echo
echo "*** installing web development platform tools..."
sudo dnf install -y node npm

echo
echo "*** preparing local git location..."
mkdir $git_code_local_dir
cd $git_code_local_dir

echo
echo "*** preparing python environment..."
mkdir venvs
cd venvs
virtualenv -p python3.7 py37
cd $git_code_local_dir

echo
echo "*** cloning necessary scripts..."
git clone "https://github.com/anandbonde/miscellaneous.git"

echo
echo "*** updating the system, this may take while..."
./miscellaneous/linux_scripts/update_fedora.sh

echo
echo "*** copying .vimrc..."
cp /home/anand/git/miscellaneous/raspberrypi_configs/.vimrc ~/.vimrc

echo
echo "*** building completer support for C..."
cd ~/.vim/plugged/youcompleteme
./install.py --clang-completer
cd

echo
echo "*** setting up terminal for future use..."
echo "" >> $bashrc_file_path
echo "# added by Anand Bonde" >> $bashrc_file_path
echo "alias v=vim" >> $bashrc_file_path
echo "alias vt='vim -p'" >> $bashrc_file_path
echo "alias u='~/git/miscellaneous/linux_scripts/update_fedora.sh'" >> $bashrc_file_path
echo "set -o vi" >> $bashrc_file_path

# activate virtualenv
echo "alias le='. ~/git/venvs/py37/bin/activate'" >> $bashrc_file_path

# face recognition
echo "alias ctr='cd ~/git/face_detector/python/src/ && python generate_training_set.py && python generate_model.py && python face_recognizer.py'" >> $bashrc_file_path
echo "alias tr='cd ~/git/face_detector/python/src/ && python generate_model.py && python face_recognizer.py'" >> $bashrc_file_path
echo "alias fr='cd ~/git/face_detector/python/src/ && python face_recognizer.py'" >> $bashrc_file_path
echo "alias clean_fd='rm -rf cd ~/git/face_detector/python/models/* cd ~/git/face_detector/python/training_data/*'" >> $bashrc_file_path

echo "*** attempting to apply these settings to the current terminal instance..."
source $bashrc_file_path

ip addr

echo "*** restart the terminal for settings to take effect for sure!"

